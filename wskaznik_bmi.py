# Wzór na BMI to Masa / Wzrost^2

mass = float(input("Podaj swoją masę w [kg]: "))
height = float(input("Podaj swój wzrost w [m]: "))

bmi = round((mass / height ** 2), 2)

print(f'BMI dla wzrostu {height} [m] i wagi {mass} [kg] wynosi: {bmi}')
